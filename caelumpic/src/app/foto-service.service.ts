import { Injectable } from '@angular/core';
import {HttpClient } from '@angular/common/http'
import { Animal } from './animal';

@Injectable({
  providedIn: 'root'
})
export class FotoServiceService {


  httpClient: HttpClient ;

  constructor(_httpClient: HttpClient ) { 
    this.httpClient = _httpClient;
  }

  private urlFotosService:string = "http://localhost:3000/v1/fotos";


  public getListaFotos( callBack:Function ):Animal[]{
    
    let listaFotos:Animal[] = null;

    this.httpClient.get(this.urlFotosService).subscribe((retorno:Animal[])=>{
      console.log("Lista de fotos:"+ retorno);
      listaFotos = retorno;
      callBack(listaFotos,null);
    }),(error)=>{
      console.log("Erro ao obter lista de fotos:"+ error);
      listaFotos = []
    }

    console.log("END:");

    return listaFotos;
  }



}
