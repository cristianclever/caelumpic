import { TestBed, inject } from '@angular/core/testing';

import { FotoServiceService } from './foto-service.service';

describe('FotoServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FotoServiceService]
    });
  });

  it('should be created', inject([FotoServiceService], (service: FotoServiceService) => {
    expect(service).toBeTruthy();
  }));
});
