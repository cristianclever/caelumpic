import { Component, OnInit } from '@angular/core';
import { Animal } from '../animal';
import { HttpClient } from '@angular/common/http';
import { FotoServiceService } from '../foto-service.service';

@Component({
  selector: 'app-listagem-component',
  templateUrl: './listagem-component.component.html',
  styleUrls: ['./listagem-component.component.css']
})
export class ListagemComponentComponent implements OnInit {



  listaFotos:Animal[];
  
  constructor(httpClient: HttpClient, fotoService:  FotoServiceService ){

    this.listaFotos = fotoService.getListaFotos((sucess,error)=>{

      if(error){
        console.error("Erro ao obter dados do serviço");
      }
      else{
        this.listaFotos = sucess;
      }
     
    });
  }



  ngOnInit() {
  }

}
