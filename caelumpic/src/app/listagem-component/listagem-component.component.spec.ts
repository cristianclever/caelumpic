import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListagemComponentComponent } from './listagem-component.component';

describe('ListagemComponentComponent', () => {
  let component: ListagemComponentComponent;
  let fixture: ComponentFixture<ListagemComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListagemComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListagemComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
