import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule } from '@angular/common/http'

import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { FotoComponent } from './foto/foto.component';
import { PainelComponentComponent } from './painel-component/painel-component.component';
import { CadastroComponentComponent } from './cadastro-component/cadastro-component.component';
import { ListagemComponentComponent } from './listagem-component/listagem-component.component';
import { AppRoutingModule } from './/app-routing.module';
import { routing } from './app-routing-routing.module';

@NgModule({
  declarations: [
    AppComponent,
    FotoComponent,
    PainelComponentComponent,
    CadastroComponentComponent,
    ListagemComponentComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    routing
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
