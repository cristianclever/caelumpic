import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListagemComponentComponent } from './listagem-component/listagem-component.component';
import { CadastroComponentComponent } from './cadastro-component/cadastro-component.component';
import { ModuleWithProviders } from '@angular/compiler/src/core';

const routes: Routes = [

  { path: '', component: ListagemComponentComponent },
  { path: 'cadastro', component: CadastroComponentComponent },
  //Rota para outros paths , o que nao acha redireciona para um dos paths... no caso ''
  { path: '**', redirectTo: '' } 


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppRoutingRoutingModule { }

export const routing:ModuleWithProviders = RouterModule.forRoot(routes);
